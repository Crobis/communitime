﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace DomainProject
{
    public class User
    {
        public int userID { get; set; }
        public string userMail { get; set; }
        public string userName { get; set; }
        public string userSurname { get; set; }
        public byte[] userKey { get; set; }
        public string userCountry { get; set; }
        public string userRegion { get; set; }
        public string userCity { get; set; }
        public int userTime { get; set; }
        public string userImagePath { get; set; }
        public bool userEmailIsConfirmed { get; set; }
        public int userConfirmationKey { get; set; }
        /*
        public ICollection<Offer> userBookmarkedOffers { get; set; }
        public ICollection<Demand> userBookmarkedDemands { get; set; }
        public ICollection<Offer> userSubmittedOffers { get; set; }
        public ICollection<Demand> userSubmittedDemands { get; set; }
        public ICollection<Message> userSentMessages { get; set; }
        public ICollection<Message> userReceivedMessages { get; set; }
        */
        public User()
        {

        }

        public User(string mail, string name, string surname, byte[] key, string country, string region, string city, string path)
        {
            userMail = mail;
            userName = name;
            userSurname = surname;
            userKey = key;
            userCountry = country;
            userRegion = region;
            userCity = city;
            userImagePath = path;
            userTime = 5;
            userEmailIsConfirmed = false;
            userConfirmationKey = 0;
        }
    }

    public class Offer
    {
        public int offerID { get; set; }
        public int userID { get; set; }
        //Poner Imagen
        public string offerName { get; set; }
        public string offerImagePath { get; set; }
        public string offerDescription { get; set; }
        public string offerTags { get; set; }
        
        //Poner categoría
        //Poner disponibilidad en días y horas
        public Offer()
        {

        }

        public Offer(int id, int userID, string name, string path, string description, string tags)
        {
            offerID = id;
            this.userID = userID;
            offerName = name;
            offerImagePath = path;
            offerDescription = description;
            offerTags = tags;
        }

        
    }

    public class Demand
    {
        public int demandID { get; set; }
        public int userID { get; set; }
        //¿poner imagen?
        public string demandName { get; set; }
        public string demandImagePath { get; set; }
        public string demandDescription { get; set; }
        public string demandTags { get; set; }
        //Poner categoría
        //Poner disponibilidad en días y horas

        public Demand()
        {

        }

        public Demand(int id, int userID, string name, string path, string description, string tags)
        {
            demandID = id;
            this.userID = userID;
            demandName = name;
            demandImagePath = path;
            demandDescription = description;
            demandTags = tags;
        }

        /*public int DemandComparison(Demand d)
        {
            List<string> tags, title;
            tags = demandTags.Split(',').ToList();
            title = demandName.Split(' ').ToList();
            int similarity = 0;



            foreach (string s in title)
                if (s.Count<char>() > 3 && !string.IsNullOrWhiteSpace(s) && (d.demandName.Contains(s) || d.demandDescription.Contains(s) || d.demandTags.Contains(s)))
                    similarity++;

            foreach (string s in tags)
                if (s.Count<char>() > 3 && !string.IsNullOrWhiteSpace(s) && (d.demandName.Contains(s) || d.demandDescription.Contains(s) || d.demandTags.Contains(s)))
                    similarity += 2;

            return similarity;
        }*/
    }

    public class Message
    {
        public int messageID { get; set; }
        public int messageFromUserID { get; set; }
        public string messageFromUserName { get; set; }
        public int chatGroupID { get; set; }
        public string messageBody { get; set; }
        public DateTime messageTimeSent { get; set; }
        public bool messageIsRead { get; set; }

        public Message() { }
        public Message(int messageFromUserID, string messageFromUserName, int chatGroupID, string messageBody, DateTime messageTimeSent)
        {
            this.messageFromUserID = messageFromUserID;
            this.messageFromUserName = messageFromUserName;
            this.chatGroupID = chatGroupID;
            this.messageBody = messageBody;
            this.messageTimeSent = messageTimeSent;
            messageIsRead = false;
        }
    }

    public class ChatGroup
    {
        public int chatGroupID { get; set; }
        public int userCreatorID { get; set; }
        public int demOrOffInt { get; set; }
        public int offerOrDemandID { get; set; }
        public string offerOrDemandName { get; set; }
        public DateTime creationDate { get; set; }
        public Message LastMessage;

        public ChatGroup () { }
        public ChatGroup(int userID, int offOrDemID, string offOrDemName, DemandOrOffer demOrOff, DateTime creationDate)
        {
            userCreatorID = userID;
            offerOrDemandID = offOrDemID;
            offerOrDemandName = offOrDemName;
            this.creationDate = creationDate;
            demOrOffInt = (int)demOrOff;
        }
    }
    public class Ul
    {
        public double lng { get; set; }
        public double lat { get; set; }
    }

    public class Lr
    {
        public double lng { get; set; }
        public double lat { get; set; }
    }

    public class BoundingBox
    {
        public Ul ul { get; set; }
        public Lr lr { get; set; }
    }

    public class LatLng
    {
        public double lng { get; set; }
        public double lat { get; set; }
    }

    public class DisplayLatLng
    {
        public double lng { get; set; }
        public double lat { get; set; }
    }

    public class Location
    {
        public LatLng latLng { get; set; }
        public string adminArea4 { get; set; }
        public string adminArea5Type { get; set; }
        public string adminArea4Type { get; set; }
        public string adminArea5 { get; set; }
        public string street { get; set; }
        public string adminArea1 { get; set; }
        public string adminArea3 { get; set; }
        public string type { get; set; }
        public DisplayLatLng displayLatLng { get; set; }
        public int linkId { get; set; }
        public string postalCode { get; set; }
        public string sideOfStreet { get; set; }
        public bool dragPoint { get; set; }
        public string adminArea1Type { get; set; }
        public string geocodeQuality { get; set; }
        public string geocodeQualityCode { get; set; }
        public string adminArea3Type { get; set; }
    }

    public class StartPoint
    {
        public double lng { get; set; }
        public double lat { get; set; }
    }

    public class Maneuver
    {
        public List<object> signs { get; set; }
        public int index { get; set; }
        public List<object> maneuverNotes { get; set; }
        public int direction { get; set; }
        public string narrative { get; set; }
        public string iconUrl { get; set; }
        public double distance { get; set; }
        public int time { get; set; }
        public List<object> linkIds { get; set; }
        public List<object> streets { get; set; }
        public int attributes { get; set; }
        public string transportMode { get; set; }
        public string formattedTime { get; set; }
        public string directionName { get; set; }
        public string mapUrl { get; set; }
        public StartPoint startPoint { get; set; }
        public int turnType { get; set; }
    }

    public class Leg
    {
        public bool hasTollRoad { get; set; }
        public int index { get; set; }
        public bool hasBridge { get; set; }
        public bool hasTunnel { get; set; }
        public List<List<object>> roadGradeStrategy { get; set; }
        public bool hasHighway { get; set; }
        public bool hasUnpaved { get; set; }
        public double distance { get; set; }
        public int time { get; set; }
        public int origIndex { get; set; }
        public bool hasSeasonalClosure { get; set; }
        public string origNarrative { get; set; }
        public bool hasCountryCross { get; set; }
        public string formattedTime { get; set; }
        public string destNarrative { get; set; }
        public int destIndex { get; set; }
        public List<Maneuver> maneuvers { get; set; }
        public bool hasFerry { get; set; }
    }

    public class RouteError
    {
        public string message { get; set; }
        public int errorCode { get; set; }
    }

    public class Options
    {
        public List<object> mustAvoidLinkIds { get; set; }
        public int drivingStyle { get; set; }
        public bool countryBoundaryDisplay { get; set; }
        public int generalize { get; set; }
        public string narrativeType { get; set; }
        public string locale { get; set; }
        public bool avoidTimedConditions { get; set; }
        public bool destinationManeuverDisplay { get; set; }
        public bool enhancedNarrative { get; set; }
        public int filterZoneFactor { get; set; }
        public int timeType { get; set; }
        public int maxWalkingDistance { get; set; }
        public string routeType { get; set; }
        public int transferPenalty { get; set; }
        public int walkingSpeed { get; set; }
        public bool stateBoundaryDisplay { get; set; }
        public int maxLinkId { get; set; }
        public List<object> arteryWeights { get; set; }
        public List<object> tryAvoidLinkIds { get; set; }
        public string unit { get; set; }
        public int routeNumber { get; set; }
        public bool doReverseGeocode { get; set; }
        public string shapeFormat { get; set; }
        public int maneuverPenalty { get; set; }
        public bool useTraffic { get; set; }
        public bool returnLinkDirections { get; set; }
        public List<object> avoidTripIds { get; set; }
        public string manmaps { get; set; }
        public int highwayEfficiency { get; set; }
        public bool sideOfStreetDisplay { get; set; }
        public int cyclingRoadFactor { get; set; }
        public int urbanAvoidFactor { get; set; }
    }

    public class Route
    {
        public bool hasTollRoad { get; set; }
        public bool hasBridge { get; set; }
        public List<object> computedWaypoints { get; set; }
        public double fuelUsed { get; set; }
        public bool hasTunnel { get; set; }
        public bool hasUnpaved { get; set; }
        public bool hasHighway { get; set; }
        public int realTime { get; set; }
        public BoundingBox boundingBox { get; set; }
        public double distance { get; set; }
        public int time { get; set; }
        public List<int> locationSequence { get; set; }
        public bool hasSeasonalClosure { get; set; }
        public string sessionId { get; set; }
        public List<Location> locations { get; set; }
        public bool hasCountryCross { get; set; }
        public List<Leg> legs { get; set; }
        public string formattedTime { get; set; }
        public RouteError routeError { get; set; }
        public Options options { get; set; }
        public bool hasFerry { get; set; }
    }

    public class Copyright
    {
        public string text { get; set; }
        public string imageUrl { get; set; }
        public string imageAltText { get; set; }
    }

    public class Info
    {
        public Copyright copyright { get; set; }
        public int statuscode { get; set; }
        public List<object> messages { get; set; }
    }

    public class RootObject
    {
        public Route route { get; set; }
        public Info info { get; set; }
    }
    public class DomainContext : DbContext
    {
        public DomainContext() : base("name=BancoTiempoDBConnection")
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<Demand> Demands { get; set; }
        public DbSet<ChatGroup> ChatGroups { get; set; }
        public DbSet<Message> Messages { get; set; }
    }

    public static class ConstantClass
    {
        public const int MaxHomeOfferDemands = 3;
        public const int MaxRecommendedOfferDemands = 3;
    }
    
    public enum DemandOrOffer { Demand, Offer }
}
