﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainProject;

namespace BancoTiempoApp.Models
{
    public class UserShowModel
    {
        public User user { get; set; }
        public int amount { get; set; }
        public List<Offer> userOffers { get; set; }
        public List<Demand> userDemands { get; set; }
    }
}