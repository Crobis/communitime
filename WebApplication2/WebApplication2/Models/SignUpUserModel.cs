﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BancoTiempoApp.Models
{
    public class SignUpUserModel
    {
        [Required(ErrorMessage = "Si us plau, escriu el teu email.")]
        [StringLength(75)]
        [Display(Name = "Email")]
        public string mail { get; set; }

        [Required(ErrorMessage = "Si us plau, escriu el teu nom.")]
        [StringLength(75)]
        [Display(Name = "Nom")]
        public string name { get; set; }

        [Required(ErrorMessage = "Si us plau, escriu els teus cognoms")]
        [StringLength(75)]
        [Display(Name = "Cognoms")]
        public string surname { get; set; }

        [Required(ErrorMessage = "Si us plau escriu la teva contrasenya")]
        [StringLength(75 , MinimumLength = 8)]
        [Display(Name = "Contrasenya")]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Required(ErrorMessage = "Si us plau torna a escriure la teva contrasenya")]
        [StringLength(75, MinimumLength = 8)]
        [Display(Name = "Torna a escriure la teva contrasenya")]
        [DataType(DataType.Password)]
        public string confirmPassword { get; set; }

        [Required(ErrorMessage = "Si us plau, escriu el teu país")]
        [StringLength(75)]
        [Display(Name = "País")]
        public string country { get; set; }

        [Required(ErrorMessage = "Si us plau, escriu la teva regió")]
        [StringLength(75)]
        [Display(Name = "Regió/Estat/Comarca")]
        public string region { get; set; }

        [Required(ErrorMessage = "Si us plau, escriu la teva ciutat")]
        [StringLength(75)]
        [Display(Name = "Ciutat")]
        public string city { get; set; }
    }
}