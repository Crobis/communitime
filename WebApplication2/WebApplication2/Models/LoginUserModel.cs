﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BancoTiempoApp.Models
{
    public class LoginUserModel
    {
        [Required(ErrorMessage = "Si us plau escriu el teu email.")]
        [StringLength(75)]
        [Display(Name = "Email")]
        public string mail { get; set; }

        [Required(ErrorMessage = "Si us plau escriu la teva contrasenya")]
        [StringLength(75)]
        [Display(Name = "Contrasenya")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}