﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainProject;

namespace BancoTiempoApp.Models
{
    public class OwnOfferShowModel
    {
        public Offer offer { get; set; }
        public List<Demand> similarDemands { get; set; } 
    }
}