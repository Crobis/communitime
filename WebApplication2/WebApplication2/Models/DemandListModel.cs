﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainProject;

namespace BancoTiempoApp.Models
{
    public class DemandListModel
    {
        public List<Demand> Demands{ get; set; }
        public string search { get; set; }
    }
}