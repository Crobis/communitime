﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainProject;
using System.ComponentModel;

namespace WebApplication2.Models
{
    public class EditOfferModel
    {
        public int id;

        public EditOfferModel() { }
        public EditOfferModel (string n, string d, string t, int ID)
        {
            title = n;
            description = d;
            tags = t;
            id = ID;
        }
        [Required(ErrorMessage = "Es necessita un títol per la oferta")]
        [StringLength(75)]
        [Display(Name = "Títol")]
        public string title { get; set; }
        

        [Required(ErrorMessage = "Es necessita una descripció per la oferta")]
        [Display(Name = "Descripció")]
        [MaxLength(1000,ErrorMessage = "Si us plau, intenta escriure 1000 caràcters o menys.")]
        [DataType(DataType.MultilineText)]
        public string description { get; set; }

        [Required(ErrorMessage = "Necessites afegir etiquetes per a què la teva oferta sigui més fàcil de trobar per altres usuaris.")]
        [Display(Name = "Etiquetes (separa les etiquetes amb comes)")]
        public string tags { get; set; }

        
    }
}