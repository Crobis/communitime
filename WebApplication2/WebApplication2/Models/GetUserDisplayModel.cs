﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainProject;

namespace BancoTiempoApp.Models
{
    public class GetUserDisplayModel
    {
        public List<Message> Messages { get; set; }
        public List<ChatGroup> Chats { get; set; }
        public int Time { get; set; }
    }
}