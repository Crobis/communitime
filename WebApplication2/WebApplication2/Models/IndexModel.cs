﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainProject;

namespace BancoTiempoApp.Models
{
    public class IndexModel
    {
        public List<Offer> offers { get; set; }
        public List<Demand> demands { get; set; }
    }
}