﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainProject;

namespace BancoTiempoApp.Models
{
    public class OfferListModel
    {
        public List<Offer> Offers { get; set; }
        public string search { get; set; }
    }
}