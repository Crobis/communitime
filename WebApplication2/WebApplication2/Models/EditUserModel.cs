﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainProject;
using System.ComponentModel;

namespace WebApplication2.Models
{
    public class EditUserModel
    {
        public int id;

        public EditUserModel() { }
        public EditUserModel (string n, string s, string c, string r, string cit, int ID)
        {
            name = n;
            surname = s;
            country = c;
            region = r;
            city = cit;
            id = ID;
        }
        [Required(ErrorMessage = "Si us plau, escriu el teu nom.")]
        [StringLength(50)]
        [Display(Name = "Nom")]
        public string name { get; set; }

        [Required(ErrorMessage = "Si us plau, escriu els teus cognoms")]
        [StringLength(50)]
        [Display(Name = "Cognoms")]
        public string surname { get; set; }

        [Required(ErrorMessage = "Si us plau, escriu el teu país")]
        [StringLength(50)]
        [Display(Name = "País")]

        public string country { get; set; }
        [Required(ErrorMessage = "Si us plau, escriu la teva regió")]
        [StringLength(50)]
        [Display(Name = "Regió/Estat/Comarca")]
        public string region { get; set; }

        [Required(ErrorMessage = "Si us plau, escriu la teva ciutat")]
        [StringLength(50)]
        [Display(Name = "Ciutat")]
        public string city { get; set; }
    }
}