﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainProject;
using System.ComponentModel;

namespace WebApplication2.Models
{
    public class ResetPasswordModel
    {
        public int id;

        public ResetPasswordModel() { }
        public ResetPasswordModel (int ID)
        {
            id = ID;
        }

        [Required(ErrorMessage = "Si us plau, escriu la teva contrasenya")]
        [StringLength(75, MinimumLength = 8)]
        [Display(Name = "Contrasenya")]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Required(ErrorMessage = "Si us plau, torna a escriure la teva contrasenya")]
        [StringLength(75, MinimumLength = 8)]
        [Display(Name ="Torna a escriure la teva contrasenya")]
        [DataType(DataType.Password)]
        public string confirmPassword { get; set; }


    }
}