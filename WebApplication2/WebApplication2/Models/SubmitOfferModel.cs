﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class SubmitOfferModel
    {
        [Required(ErrorMessage = "Es necessita un títol per a l'oferta.")]
        [StringLength(75)]
        [Display(Name = "Títol")]
        public string title { get; set; }

        [Required(ErrorMessage = "Es necessita una descripció per a l'oferta.")]
        [Display(Name = "Descripció")]
        [MaxLength(1000,ErrorMessage = "Si us plau, intenta escriure 1000 caràcters o menys-")]
        [DataType(DataType.MultilineText)]
        public string description { get; set; }

        [Required(ErrorMessage = "Necessites afegir etiquetes per fer que la teva oferta sigui més fàcil de trobar.")]
        [Display(Name = "Etiquetes (separa les etiquetes amb comes)")]
        public string tags { get; set; }
    }
}