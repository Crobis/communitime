﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainProject;

namespace BancoTiempoApp.Models
{
    public class ChatGroupModel
    {
        public ChatGroup chatGroup { get; set; }
        public string userName { get; set; }
        public int userID { get; set; }
        public List<Message> chatMessages { get; set; }
        public string offerDemandName { get; set; }
        public string offerDemandImagePath { get; set; }
        public string offerDemandDescription { get; set; }
    }
}