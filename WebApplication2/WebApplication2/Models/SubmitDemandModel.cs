﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class SubmitDemandModel
    {
        [Required(ErrorMessage = "Es necessita un títol per la demanda.")]
        [StringLength(75)]
        [Display(Name = "Títol")]
        public string title { get; set; }

        [Required(ErrorMessage = "Es necessita una descripció per la demanda.")]
        [Display(Name = "Descripció")]
        [MaxLength(1000, ErrorMessage = "Si us plau, intenta escriure 1000 caràcters o menys.")]
        [DataType(DataType.MultilineText)]
        public string description { get; set; }

        [Required(ErrorMessage = "Necessites etiquetes per a què la teva demanda sigui més fàcil de trobar per altres usuaris.")]
        [Display(Name = "Etiquetes (separa les etiquetes amb comes)")]
        public string tags { get; set; }
    }
}