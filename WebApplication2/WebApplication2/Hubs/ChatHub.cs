﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using DomainProject;
using WebApplication2.Controllers;
using Business;

namespace WebApplication2.Hubs
{
    public class ChatHub : Hub
    {
        public void Send(string name, string body, int groupID, int userID)
        {
            //Crida el mètode addNewMessageToPage per afegir el missatge als usuaris del xat
            Clients.Group(groupID.ToString()).addNewMessageToPage(name, body);

            //Guarda el missatge a la seva base de dades
            Message message = new Message(userID, name, groupID, body, DateTime.Now);
            MessageManager.SaveMessage(message);
        }
        public Task StartGroup(int groupID)
        {
            //Afegeix l'usuari al xat
            return Groups.Add(Context.ConnectionId, groupID.ToString());
        }
    }
}