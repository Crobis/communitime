﻿using Microsoft.Owin;
using Owin;
using BancoTiempoApp;

[assembly: OwinStartup(typeof(BancoTiempoApp.Startup))]
namespace BancoTiempoApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
