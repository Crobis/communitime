﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business;

namespace WebApplication2.Infrastructure
{
    public class AuthorizationFilter : System.Web.Mvc.ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            try
            {
                if (HttpContext.Current.Request.Cookies["AuthID"].Value != HttpContext.Current.Session["AuthID"].ToString() || !UserManager.GetUser(System.Convert.ToInt32(HttpContext.Current.Request.Cookies["AuthID"].Value)).userEmailIsConfirmed)
                {
                    actionContext.Result = new RedirectResult("/User/LoginUser/");
                }
            }
            catch
            {
                actionContext.Result = new RedirectResult("/User/LoginUser/");
            }
        }
    }
}