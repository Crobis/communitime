﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Infrastructure;
using DomainProject;
using Business;
using BancoTiempoApp.Models;

namespace BancoTiempoApp.Controllers
{
    public class HomeController : Controller
    {
        [AuthorizationFilter]
        public ActionResult Index()
        {
            //Es crea un model i s'emmagatzemen les demandes i ofertes que s'ensenyaran a la pàgina principal
            IndexModel iModel = new IndexModel();
            iModel.demands = DemandManager.GetHomeDemands();
            iModel.offers = OfferManager.GetHomeOffers();

            //Es passa el model al view
            return View(iModel);
        }
        [AuthorizationFilter]
        public ActionResult Contact()
        {
            //S'assigna un missatge al viewbag
            ViewBag.Message = "Contacta amb nosaltres";

            return View();
        }
        
    }
}
