﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainProject;
using System.Security.Cryptography;
using Business;
using WebApplication2.Infrastructure;
using BancoTiempoApp.Models;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class DemandController : BaseController
    {
        RNGCryptoServiceProvider rngCSP = new RNGCryptoServiceProvider();
        // GET: Demand
        [AuthorizationFilter]
        public ActionResult Index()
        {
            return RedirectToAction("SubmitDemand");
        }
        [AuthorizationFilter]
        public ActionResult SubmitDemand()
        {
            return View();
        }
        [AuthorizationFilter]
        public ActionResult DemandSubmitted(SubmitDemandModel demModel, HttpPostedFileBase file)
        {
            //Dóna un nom únic a l'imatge pujada per l'usuari i la guarda al servidor
            string fileExtension = System.IO.Path.GetExtension(file.FileName);
            string pic = "demand" + demModel.GetHashCode().ToString() + file.GetHashCode().ToString() + fileExtension;
            string path = System.IO.Path.Combine(Server.MapPath("~/Content/Images/"), pic);
            file.SaveAs(path);

            //Es crea la demanda a partir de les dades del model i es guarda a la base de dades
            Demand demand = new Demand(rngCSP.GetHashCode(), GetLoggedInUserId(), demModel.title, pic, demModel.description, demModel.tags);
            DemandManager.SaveDemand(demand);
            return View();
        }
        [AuthorizationFilter]
        public ActionResult DemandList(DemandListModel dModel)
        {
            //Es busquen les demandes que coincideixin amb la cerca (totes si la barra de cerca està en blanc)
            //i s'emmagatzemen al model
            dModel.Demands = DemandManager.GetDemands(dModel.search);

            //S'ordenen per proximitat a l'usuari
            User user = UserManager.GetUser(GetLoggedInUserId());
            dModel.Demands = dModel.Demands.OrderBy(d => DemandManager.GetDistance(user, d)).ToList();

            //Passa el model al view
            return View("DemandList", dModel);
        }
        [AuthorizationFilter]
        public ActionResult DemandShow(int id)
        {
            //Si la demanda ha estat creada per l'usuari:
            if (DemandManager.GetDemand(id).userID == GetLoggedInUserId())
                //Utilitza el mètode per mostrar la pròpia demanda
                return RedirectToAction("OwnDemandShow", new { id = id });
            //Si no:
            else
            {
                //Crea un model amb la demanda i demandes similars a ella
                DemandShowModel dModel = new DemandShowModel();
                dModel.demand = DemandManager.GetDemand(id);
                dModel.similarDemands = DemandManager.GetSimilarDemands(dModel.demand);

                //Passa el model al view
                return View(dModel);
            }
        }
        [AuthorizationFilter]
        public ActionResult OwnDemandShow(int id)
        {
            //Si la demanda ha estat creada per l'usuari:
            if (DemandManager.GetDemand(id).userID == GetLoggedInUserId())
            {
                //Emmagatzema la demanda i ofertes similars al model
                OwnDemandShowModel dModel = new OwnDemandShowModel();
                dModel.demand = DemandManager.GetDemand(id);
                dModel.similarOffers = DemandManager.GetSimilarOffers(dModel.demand);

                //Passa el model al view
                return View(dModel);

            }
            //Si no:
            else
                //Utilitza el mètode per demandes que no són de l'usuari
                return RedirectToAction("DemandShow", new { id = id });
        }
        [AuthorizationFilter]
        public ActionResult EditDemand(int id)
        {
            //Si la demanda ha estat creada per l'usuari:
            if (DemandManager.GetDemand(id).userID == GetLoggedInUserId())
            {
                //Passa les dades de la demanda a editar a un model
                Demand demand = DemandManager.GetDemand(id);
                EditDemandModel dModel = new EditDemandModel(demand.demandName, demand.demandDescription, demand.demandTags, id);

                //Passa el model al view
                return View(dModel);
            }
            //Si no:
            else
                //No permetis que l'editi, redirigeix-lo a la pàgina on es mostra la demanda
                return RedirectToAction("DemandShow", new { id = id });
        }
        public ActionResult DemandEditted(EditDemandModel dModel, HttpPostedFileBase file, int id)
        {
            string pic;
            //Si s'ha pujat alguna imatge:
            if (file != null)
            {
                //Donar nom únic a la imatge i guardarla al servidor
                string fileExtension = System.IO.Path.GetExtension(file.FileName);
                pic = "demand" + dModel.GetHashCode().ToString() + file.GetHashCode().ToString() + fileExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/Content/Images/"), pic);
                file.SaveAs(path);
            }
            //Si no:
            else
            {
                //S'assigna el valor ja existent del directori de la imatge a "pic"
                pic = DemandManager.GetDemand(id).demandImagePath;
            }

            //Es crea una nova demanda amb els canvis de l'edició i es substitueix la versió antiga
            Demand demand;
            demand = new Demand(id, DemandManager.GetDemand(id).userID, dModel.title, pic, dModel.description, dModel.tags);
            DemandManager.EditDemand(demand);

            //Es redirigeix a la pàgina on es mostra la demanda perquè es puguin apreciar els canvis
            return RedirectToAction("DemandShow", "Demand", new { id = id });
        }
        [AuthorizationFilter]
        public ActionResult DemandChat(int demandID)
        {
            int chatGroupID;

            //Si encara no s'ha creat cap xat en aquesta demanda amb aquest usuari:
            if (!GroupManager.DoesGroupExist(demandID, DemandOrOffer.Demand, GetLoggedInUserId()))
            {
                //Crea un nou xat en aquesta demanda amb aquest usuari i guarda'l a la base de dades
                ChatGroup group = new ChatGroup(GetLoggedInUserId(), demandID, DemandManager.GetDemand(demandID).demandName, DemandOrOffer.Demand, DateTime.Now);
                GroupManager.SaveGroup(group);
            }
            //Busca un xat en aquesta demanda amb aquest usuari (que existeix o bé perquè s'acaba de crear o perquè ja existia)
            chatGroupID = GroupManager.GetGroup(demandID, DemandOrOffer.Demand, GetLoggedInUserId()).chatGroupID;

            //Redirigeix a la pàgina de mostra d'aquest xat
            return RedirectToAction("ChatShow", "Chat", new { id = chatGroupID });
        }
        [AuthorizationFilter]
        public ActionResult SearchDemand(string demandSearch)
        {
            //Guarda la cerca a un model DemandList, i passa aquest model al view DemandList
            DemandListModel dModel1 = new DemandListModel();
            dModel1.search = demandSearch;
            return DemandList(dModel1);
        }
    }
}