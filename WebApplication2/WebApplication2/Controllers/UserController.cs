﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Web.Mvc;
using DomainProject;
using BancoTiempoApp.Models;
using Business;
using Microsoft.Web.Infrastructure;
using WebApplication2.Controllers;
using WebApplication2.Infrastructure;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace BancoTiempoApp.Controllers
{

    public class UserController : BaseController
    {
        RNGCryptoServiceProvider rngCSP = new RNGCryptoServiceProvider();
        
        // GET: User

        public ActionResult Index()
        {
            return RedirectToAction("AddUser");
        }
        public ActionResult AddUser()
        {
            var model = new SignUpUserModel();
            return View(model);
        }
        public ActionResult UserAdded(SignUpUserModel UModel, HttpPostedFileBase file)
        {
            //Si ha escrit bé la contrasenya les dues vegades:
            if (UModel.password == UModel.confirmPassword)
            {
                //Dóna un nom únic a l'imatge pujada per l'usuari i guarda-la al servidor
                string fileExtension = System.IO.Path.GetExtension(file.FileName);
                string pic = "user" + UModel.GetHashCode().ToString() + file.GetHashCode().ToString() + fileExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/Content/Images/user"), pic);
                file.SaveAs(path);

                //Crea una clau encriptada a partir de la contrasenya de l'usuari
                byte[] key = PasswordManager.Hash(UModel.password);
                //Crea un nou usuari amb les propietats rebudes del model i guarda'l a la base de dades
                User user = new User(UModel.mail, UModel.name, UModel.surname, key, UModel.country, UModel.region, UModel.city, pic);
                UserManager.SaveUser(user);
                //Envia un email de confirmació a l'usuari
                SendConfirmationEmail(UserManager.GetUserID(user.userMail));
                return View();
            }
            //Si les contrasenyes no coincideixen, retorna a la pàgina anterior
            return View("AddUser");
        }
        public async void SendConfirmationEmail(int id)
        {
            //Genera un codi de confirmació únic per l'usuari
            int code = UserManager.GenerateEmailConfirmationToken(id);
            //Crea un enllaç que inclou el codi i la id de l'usuari
            var callbackUrl = Url.Action("ConfirmEmail", "User",
                new { userId = id, code = code }, protocol: Request.Url.Scheme);
            //Envia un email a l'usuari amb l'enllaç per confirmar el seu compte
            await UserManager.SendConfirmationEmail(id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
        }
        public ActionResult ResendConfirmationEmail()
        {
            return View();
        }
        public ActionResult ConfirmationEmailSent(string email)
        {
            //Si l'email és vàlid
            if (UserManager.IsUserValid(email))
            {
                //Envia un email de confirmació a l'usuari
                SendConfirmationEmail(UserManager.GetUserID(email));
                return View();
            }
            //Si no:
            else
            {
                //Mostra un error dient que l'email no és vàlid
                ViewBag.ErrorMessage = "Aquesta adreça no pertany a cap usuari registrat";
                return View("Error", "Shared");
            }
        }
        public ActionResult ForgotPassword()
        {
            return View();
        }
        public async Task<ActionResult> PasswordResetEmailSent(string email)
        {
            //Guarda la id de l'usuari a una variable
            int id = UserManager.GetUserID(email);
            //Crea un codi únic per l'usuari
            int code = UserManager.GenerateEmailConfirmationToken(id);
            //Crea un enllaç que inclou la id i el codi de l'usuari
            var callbackUrl = Url.Action("ResetPassword", "User",
                new { userId = id, code = code }, protocol: Request.Url.Scheme);
            //Envia un email a l'usuari amb l'enllaç per restablir la seva contrasenya
            await UserManager.SendConfirmationEmail(id, "Reset your password", "If you want to reset your password, click <a href=\"" + callbackUrl + "\">here</a>, in case you didn't forget your password, just ignore this email.");
            return View();
        }
        public ActionResult ResetPassword(int userId, int code)
        {
            bool result = UserManager.ConfirmEmail(userId, code);
            //Si el codi coincideix amb la id de l'usuari
            if (result)
            {
                //Assigna true al TempData RedirectCall per poder accedir a l'acció següent
                TempData["RedirectCall"] = true;
                //Crea un ResetPasswordModel per l'usuari
                ResetPasswordModel pModel = new ResetPasswordModel(userId);
                return View(pModel);
            }
            //Si no:
            else
            {
                //Mostra un error que digui que la clau no coincideix amb l'usuari
                ViewBag.ErrorMessage = "La clau de confirmació no coincideix, intenta reenviar l'email. Si segueix sense funcionar, si us plau contacta l'administrador de la web: suportcommunitime@gmail.com";
                return View("Error");
            }
            
        }
        public ActionResult PasswordReset(int userId, ResetPasswordModel model)
        {
            //Si la contrasenya coincideix i s'ha accedit a aquest mètode desde ResetPassword() i no desde la barra del navegador:
            if (model.password == model.confirmPassword && TempData["RedirectCall"] != null)
            {
                //Canvia la contrasenya de l'usuari
                UserManager.ChangePassword(userId, model.password);
                return View();
            }
            //Si no:
            else
            {
                //Retorna a la pàgina anterior
                model.confirmPassword = "";
                return View("ResetPassword",model);
            }
            
        }
        public ActionResult LoginUser()
        {
            LoginUserModel uModel = new LoginUserModel();
            return View(uModel);
        }
        public ActionResult LogoutUser()
        {
            //Elimina les cookies de la sessió de l'usuari
            Session.Remove("AuthID");
            Request.Cookies.Remove("AuthID");
            return RedirectToAction("LoginUser");
        }
        public ActionResult CheckUser(LoginUserModel uModel)
        {
            //Si l'usuari que s'ha escrit és vàlid i la contrasenya coincideix amb l'usuari:
            if (UserManager.IsUserValid(uModel.mail) && PasswordManager.IsPasswordValid(UserManager.GetUser(uModel.mail), uModel.Password))
            {
                //Afegeix una sessió per l'usuari
                string authID = UserManager.GetUserID(uModel.mail).ToString();
                Session["AuthID"] = authID;

                //Afegeix una cookie que guardi les dades de l'usuari
                var cookie = new HttpCookie("AuthID");
                cookie.Value = authID;
                Response.Cookies.Add(cookie);

                //Redirigeix a la pàgina principal
                return RedirectToAction("Index", "Home");
            }
            else return RedirectToAction("LoginUser");
        }
        [AuthorizationFilter]
        public ActionResult UserList(UserListModel listModel)
        {
            //Agafa una llista de tots els usuaris
            listModel.Users = UserManager.GetUsers();
            //Passa les dades al model
            return View(listModel);
        }
        [AuthorizationFilter]
        public ActionResult UserShow(int id)
        {
            //Crea un nou UserShowModel i afegeix-li les propietats
            UserShowModel uModel = new UserShowModel();
            uModel.user = UserManager.GetUser(id);
            uModel.userOffers = UserManager.GetUserOffers(id);
            uModel.userDemands = UserManager.GetUserDemands(id);
            //Passa el model al view
            return View(uModel);
        }
        [AuthorizationFilter]
        public ActionResult UserAccount()
        {
            //Crea un nou UserShowModel i afegeix-li les propietats de l'usuari connectat
            UserShowModel uModel = new UserShowModel();
            uModel.user = UserManager.GetUser(GetLoggedInUserId());
            uModel.userOffers = UserManager.GetUserOffers(uModel.user.userID);
            uModel.userDemands = UserManager.GetUserDemands(uModel.user.userID);
            //Passa el model al view
            return View(uModel);
        }
        [AuthorizationFilter]
        public ActionResult EditUser(int id)
        {
            //Si l'usuari que es vol editar és l'usuari connectat:
            if (id == GetLoggedInUserId())
            {
                //Crea un now EditUserModel amb les propietats de l'usuari connectat
                User user = UserManager.GetUser(id);
                EditUserModel uModel = new EditUserModel(user.userName,
                    user.userSurname,
                    user.userCountry,
                    user.userRegion,
                    user.userCity,
                    user.userID);
                //Passa el model al view
                return View(uModel);
            }
            //Si no:
            else
                //No permetis que s'editi l'usuari, redirigeix a la pàgina de mostra de l'usuari
                return RedirectToAction("UserShow", new { id = id });
        }
        public ActionResult UserEditted(EditUserModel uModel, HttpPostedFileBase file, int id)
        {
            User user = UserManager.GetUser(id);
            //Si s'ha pujat una imatge
            if (file != null)
            {
                //Dóna un nom únic a la imatge i guarda-la al servidor
                string fileExtension = System.IO.Path.GetExtension(file.FileName);
                user.userImagePath = "user" + uModel.GetHashCode().ToString() + file.GetHashCode().ToString() + fileExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/Content/Images/"), user.userImagePath);
                file.SaveAs(path);
            }

            //Guarda les propietats de l'usuari
            user.userName = uModel.name; user.userSurname = uModel.surname;
            user.userCountry = uModel.country;  user.userRegion = uModel.region; user.userCity = uModel.country;
            //Edita l'usuari
            UserManager.EditUser(user);
            //Redirigeix a la pàgina de mostra del propi usuari per apreciar els canvis
            return RedirectToAction("UserAccount");
        }
        public ActionResult PayUser(int userID, int amount)
        {
            //Executa un pagament entre dos usuaris
            UserManager.PayUser(GetLoggedInUserId(), userID, amount);
            //Redirigeix a la pàgina de mostra de l'usuari destinatari
            return RedirectToAction("UserShow", new { id = userID });
        }
        public async Task<ActionResult> ConfirmEmail(int userId, int code)
        {
            //Espera 1 segon
            await Task.Delay(1000);
            //Mira si el codi coincideix amb l'usuari
            var result = UserManager.ConfirmEmail(userId, code);
            ViewBag.ErrorMessage = "La clau de confirmació no coincideix, intenta reenviar l'email. Si segueix sense funcionar, si us plau contacta l'administrador de la web: suportcommunitime@gmail.com";
            //Si coincideix, torna a la pàgina d'entrada. Si no, mostra un error
            return View(result ? "LoginUser" : "Error");
        }
        public async Task<ActionResult> SendContactEmail (string subject, string body)
        {
            //Envia el missatge de contacte al correu de suport
            await UserManager.SendContactEmail(GetLoggedInUserId(), subject, body);
            return View();
        }
    }
}