﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class BaseController : Controller
    {
        // Agafa l'ID de l'usuari connectat
        public int GetLoggedInUserId()
        {
            try
            {
                return Int32.Parse(Request.Cookies["AuthID"].Value);
            }
            catch
            {
                throw new Exception("Could not find logged in user!!");
            }
        }
    }
}