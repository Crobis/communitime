﻿using WebApplication2.Infrastructure;
using DomainProject;
using Business;
using WebApplication2.Models;
using System.Security.Cryptography;
using System.Web.Mvc;
using BancoTiempoApp.Models;
using System;
using System.Web;
using System.Linq;

namespace WebApplication2.Controllers
{
    public class OfferController : BaseController
    {
        RNGCryptoServiceProvider rngCSP = new RNGCryptoServiceProvider();
        // GET: Offer
        [AuthorizationFilter]
        public ActionResult Index()
        {
            return RedirectToAction("OfferList");
        }

        [AuthorizationFilter]
        public ActionResult SubmitOffer()
        {
            return View();
        }
        [AuthorizationFilter]
        public ActionResult OfferSubmitted(SubmitOfferModel offmodel, HttpPostedFileBase file)
        {
            //Dóna un nom únic a l'imatge pujada per l'usuari i la guarda al servidor
            string fileExtension = System.IO.Path.GetExtension(file.FileName);
            string pic = "offer" + offmodel.GetHashCode().ToString() + file.GetHashCode().ToString() + fileExtension;
            string path = System.IO.Path.Combine(Server.MapPath("~/Content/Images/"), pic);
            file.SaveAs(path);

            //Es crea l'oferta a partir de les dades del model i es guarda a la base de dades
            Offer offer = new Offer(rngCSP.GetHashCode(), GetLoggedInUserId(), offmodel.title, pic, offmodel.description, offmodel.tags);
            OfferManager.SaveOffer(offer);
            return View();
        }

        [AuthorizationFilter]
        public ActionResult OfferList(OfferListModel oModel)
        {
            //Es busquen les ofertes que coincideixin amb la cerca (totes si la barra de cerca està en blanc)
            //i s'emmagatzemen al model
            oModel.Offers = OfferManager.GetOffers(oModel.search);

            //S'ordenen per proximitat a l'usuari
            User user = UserManager.GetUser(GetLoggedInUserId());
            oModel.Offers = oModel.Offers.OrderBy(o => OfferManager.GetDistance(user, o)).ToList();

            //Passa el model al view
            return View("OfferList", oModel);
        }
        [AuthorizationFilter]
        public ActionResult OfferShow(int id)
        {
            //Si l'oferta ha estat creada per l'usuari:
            if (OfferManager.GetOffer(id).userID == GetLoggedInUserId())
                //Utilitza el mètode per mostrar la pròpia oferta
                return RedirectToAction("OwnOfferShow", new { id = id });
            //Si no:
            else
            {
                //Crea un model amb l'oferta i ofertes similars a ella
                OfferShowModel oModel = new OfferShowModel();
                oModel.offer = OfferManager.GetOffer(id);
                oModel.similarOffers = OfferManager.GetSimilarOffers(oModel.offer);

                //Passa el model al view
                return View(oModel);
            }
        }
        [AuthorizationFilter]
        public ActionResult OwnOfferShow(int id)
        {
            //Si l'oferta ha estat creada per l'usuari:
            if (OfferManager.GetOffer(id).userID == GetLoggedInUserId())
            {
                //Emmagatzema l'oferta i demandes similars al model
                OwnOfferShowModel oModel = new OwnOfferShowModel();
                oModel.offer = OfferManager.GetOffer(id);
                oModel.similarDemands = OfferManager.GetSimilarDemands(oModel.offer);

                //Passa el model al view
                return View(oModel);
            }
            //Si no:
            else
                //Utilitza el mètode per ofertes que no són de l'suari
                return RedirectToAction("OfferShow", new { id = id });
        }
        [AuthorizationFilter]
        public ActionResult EditOffer(int id)
        {
            //Si l'oferta ha estat creada per l'usuari
            if (OfferManager.GetOffer(id).userID == GetLoggedInUserId())
            {
                //Passa les dades de l'oferta a editar a un model
                Offer offer = OfferManager.GetOffer(id);
                EditOfferModel oModel = new EditOfferModel(offer.offerName, offer.offerDescription, offer.offerTags, id);

                //Passa el model al view
                return View(oModel);
            }
            //Si no:
            else
                //No permeti que l'editi, redirigeix-lo a la pàgina on es mostra l'oferta
                return RedirectToAction("OfferShow", new { id = id });
        }
        public ActionResult OfferEditted(EditOfferModel oModel, HttpPostedFileBase file, int id)
        {
            string pic;
            //Si s'ha pujat alguna imatge:
            if (file != null)
            {
                //Dóna un nom únic a la imatge i guarda-la al servidor
                string fileExtension = System.IO.Path.GetExtension(file.FileName);
                pic = "offer" + oModel.GetHashCode().ToString() + file.GetHashCode().ToString() + fileExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/Content/Images/"), pic);
                file.SaveAs(path);
            }
            //Si no:
            else
            {
                //S'assigna el valor ha existent del directori de la imatge a "pic"
                pic = OfferManager.GetOffer(id).offerImagePath;
            }

            //Es crea una nova oferta amb els canvis de l'edició i es substitueix la versió antiga
            Offer offer;
            offer = new Offer(id, OfferManager.GetOffer(id).userID, oModel.title, pic, oModel.description, oModel.tags);
            OfferManager.EditOffer(offer);

            //Es redirigeix a la pàgina on es mostra l'oferta perquè es puguin apreciar els canvis
            return RedirectToAction("OfferShow", "Offer", new { id = id });
        }
        [AuthorizationFilter]
        public ActionResult OfferChat(int offerID)
        {
            int chatGroupID;
            //Si encara no s'ha creat cap xat en aquesta oferta amb aquest usuari:
            if (!GroupManager.DoesGroupExist(offerID, DemandOrOffer.Offer, GetLoggedInUserId()))
            {
                //Crea un nou xat en aquesta oferta amb aquest usuari i guarda'l a la base de dades
                ChatGroup group = new ChatGroup(GetLoggedInUserId(), offerID, OfferManager.GetOffer(offerID).offerName, DemandOrOffer.Offer, DateTime.Now);
                GroupManager.SaveGroup(group);
            }
            //Busc un xat en aquesta oferta amb aquest usuari (que existeix o bé perquè s'acaba de crear o perquè ja existia)
            chatGroupID = GroupManager.GetGroup(offerID, DemandOrOffer.Offer, GetLoggedInUserId()).chatGroupID;

            //Redirigeix a la pàgina de mostra d'aquest xat
            return RedirectToAction("ChatShow", "Chat", new { id = chatGroupID });

        }
        [AuthorizationFilter]
        public ActionResult SearchOffer(string offerSearch)
        {
            //Guarda la cerca a un model OfferList, i passa aquest model al view OfferList
            OfferListModel oModel1 = new OfferListModel();
            oModel1.search = offerSearch;
            return OfferList(oModel1);

        }
    }
}