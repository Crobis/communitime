﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business;
using BancoTiempoApp.Models;

namespace WebApplication2.Controllers
{
    public class SharedController : BaseController
    {
        // GET: Partial
        public ActionResult Index()
        {
            return View("Index","Home");
        }

        [ChildActionOnly]
        public ActionResult GetUserDisplay()
        {
            int userID = GetLoggedInUserId();
            GetUserDisplayModel mModel = new GetUserDisplayModel();
            mModel.Messages = MessageManager.GetNewMessages(userID);
            mModel.Chats = GroupManager.GetGroups(userID);
            foreach (var chat in mModel.Chats)
                chat.LastMessage = GroupManager.GetLastMessage(chat.chatGroupID);
            mModel.Chats = mModel.Chats.OrderBy(g => g.LastMessage.messageTimeSent).ToList();
            mModel.Time = UserManager.GetUser(userID).userTime;
            return View(mModel);
        }
    }
}