﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BancoTiempoApp.Models;
using Business;
using DomainProject;


namespace WebApplication2.Controllers
{
    public class ChatController : BaseController
    {
        // GET: Chat
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ChatShow(int id)
        {
            //Crea un nou grup de xat i l'emmagatzema al model
            ChatGroupModel cgModel = new ChatGroupModel();
            cgModel.chatGroup = GroupManager.GetGroup(id);

            //Comproba si l'usuari té permís per accedir al xat
            if (cgModel.chatGroup.userCreatorID == GetLoggedInUserId() ||
                (OfferManager.GetOffer(cgModel.chatGroup.offerOrDemandID).userID == GetLoggedInUserId() && cgModel.chatGroup.demOrOffInt == (int)DemandOrOffer.Offer) ||
                (DemandManager.GetDemand(cgModel.chatGroup.offerOrDemandID).userID == GetLoggedInUserId() && cgModel.chatGroup.demOrOffInt == (int)DemandOrOffer.Demand))
            {
                //Emmagatzema totes les dades necessàries al model
                string[] info = GroupManager.GetOfferOrDemandInfo(cgModel.chatGroup);
                cgModel.offerDemandName = info[0];
                cgModel.offerDemandImagePath = info[1];
                cgModel.offerDemandDescription = info[2];
                cgModel.chatMessages = MessageManager.GetMessages(id);
                cgModel.chatMessages.Reverse();
                cgModel.userName = UserManager.GetUser(GetLoggedInUserId()).userName;
                cgModel.userID = GetLoggedInUserId();
                GroupManager.ReadMessages(id, cgModel.userID);
                
                //Passa el model al view
                return View(cgModel);
            } else
            {
                //Si no té permís, es redirigeix l'usuari a la pàgina principal
                return View("Home", "Index");
            }
           
        }
    }
}