﻿using DomainProject;
using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity.Infrastructure;
using System.Security.Cryptography;
using System.Net;
using System.Net.Mail;
using SendGrid;
using NLog.Internal;
using System.Diagnostics;

namespace Business
{
    public static class UserManager
    {
        static Random rngCSP = new Random();
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public static void SaveUser(User user)
        {
            //Utilizant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
            {
                //Si l'usuari existeix:
                if (DoesUserExist(user))
                    //Llençar un error indicant que l'usuari ja existeix
                    throw new Exception("That user is already registered");
                //Si l'usuari no existeix:
                else
                {
                    //Afegeix l'usuari a la base de dades i desa els canvis
                    ctx.Users.Add(user);
                    ctx.SaveChanges();
                }
            }
        }
        public static bool DoesUserExist(User user)
        {
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
                //Retorna true si existeix un usuari com el que ha rebut el mètode. False si no
                return ctx.Users.Any(n => n.userMail == user.userMail || n.userName == user.userName);
        }
        public static bool IsUserValid(string email)
        {
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
                //Returna true si algun usuari té l'email, false si no.
                return ctx.Users.Any(n => n.userMail == email);
        }
        public static int GetUserID(string email)
        {
            //Declara un nou usuari
            User user = new User();
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
            {
                //Assigna l'usuari que tingui aquest email a "user"
                user = ctx.Users.FirstOrDefault(n => n.userMail == email);
            }
            //Retorna la id de "user"
            return user.userID;
        }
        public static User GetUser(int id)
        {
            //Declara un nou usuari
            User user = new User();
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
                //L'usuari és igual al que tingui la "userID" igual a "id"
                user = ctx.Users.FirstOrDefault(u => u.userID == id);
            //Retorna l'usuari
            return user;
        }
        public static User GetUser(string mail)
        {
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
                //Retorna l'usuari que tingui l'email rebut pel mètode
                return ctx.Users.FirstOrDefault(u => u.userMail == mail);
        }
        public static List<User> GetUsers()
        {
            //Declara una nova llista d'usuaris
            List<User> users = new List<User>();

            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
                //La llista és igual a tots els usuaris
                users = ctx.Users.ToList();
            //Retorna la llista
            return users;
        }
        public static List<Offer> GetUserOffers(int id)
        {
            //Declara una nova llista d'ofertes
            List<Offer> offers = new List<Offer>();
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
                //La llista d'ofertes és igual a les ofertes creades per l'usuari
                offers = ctx.Offers.Where(o => o.userID == id).ToList();
            //Retorna la llista d'ofertes
            return offers;
        }
        public static List<Demand> GetUserDemands(int id)
        {
            //Declara una nova llista d'ofertes
            List<Demand> demands = new List<Demand>();
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
                //La llista de demandes és igual a les demandes creades per l'usuari
                demands = ctx.Demands.Where(d => d.userID == id).ToList();
            //Retorna la llista de demandes
            return demands;
        }
        public static void EditUser(User u)
        {
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
            {
                //Canvia tots els valors antics de l'usuari pels paràmetres nous rebuts des del controller
                System.IO.File.Delete(ctx.Users.FirstOrDefault(us => us.userID == u.userID).userImagePath);
                ctx.Users.FirstOrDefault(us => us.userID == u.userID).userName = u.userName;
                ctx.Users.FirstOrDefault(us => us.userID == u.userID).userSurname = u.userSurname;
                ctx.Users.FirstOrDefault(us => us.userID == u.userID).userImagePath = u.userImagePath;
                ctx.Users.FirstOrDefault(us => us.userID == u.userID).userCountry = u.userCountry;
                ctx.Users.FirstOrDefault(us => us.userID == u.userID).userRegion = u.userRegion;
                ctx.Users.FirstOrDefault(us => us.userID == u.userID).userCity = u.userCity;
                //Desa els canvis
                ctx.SaveChanges();
            }
        }
        public static void PayUser(int userID1, int userID2, int amount)
        {
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
            {
                //Si l'usuari té el suficient temps per pagar la quantitat "amount", i "amount" és major que 0:
                if (amount < ctx.Users.FirstOrDefault(u => u.userID == userID1).userTime && amount > 0)
                {
                    //Resta "amount" al temps del remitent i suma "amount" al temps del destinatari
                    ctx.Users.FirstOrDefault(u => u.userID == userID1).userTime -= amount;
                    ctx.Users.FirstOrDefault(u => u.userID == userID2).userTime += amount;
                    //Desa els canvis
                    ctx.SaveChanges();
                }
            }
        }
        public static bool ConfirmEmail(int id, int key)
        {
            //Declara un nou usuari a partir de "id"
            User user = GetUser(id);
            //Si la clau coincideix amb la que se li ha enviat per email:
            if (user.userConfirmationKey == key)
                //Utilitzant l'accés "ctx" a la base de dades
                using (var ctx = new DomainContext())
                {
                    //Marca l'adreça de correu electrònic de l'usuari com a confirmada
                    ctx.Users.FirstOrDefault(u => u.userID == id).userEmailIsConfirmed = true;
                    //Desa els canvis
                    ctx.SaveChanges();
                }
            //Retorna true si l'usuari ha estat confirmat, false en cas contrari
            return user.userConfirmationKey == key;
        }
        public static int GenerateEmailConfirmationToken(int id)
        {
            //Crea una nova clau aleatoria 
            var key = rngCSP.Next();
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
            {
                //Assigna la clau a l'usuari
                ctx.Users.FirstOrDefault(u => u.userID == id).userConfirmationKey = key;
                //Desa els canvis
                ctx.SaveChanges();
            }
            //Retorna la clau
            return key;
        }
        public static async Task SendConfirmationEmail(int id, string subject, string body)
        {
            //Crea l'objecte de l'email
            var myMessage = new SendGridMessage();

            //Crea l'usuari destinatari
            User recipientUser = GetUser(id);

            //Afegeix les propietats al missatge
            myMessage.From = new MailAddress("noreply@comtime");

            string recipientAddress = recipientUser.userName + " " + recipientUser.userSurname + " <" + recipientUser.userMail + ">";

            myMessage.AddTo(recipientAddress);

            myMessage.Subject = subject;

            //Afegeix el cos en text i en HTML
            myMessage.Html = body;
            myMessage.Text = body;

            //Introdueix les credencials de l'aplicació externa SendGrid
            var credentials = new NetworkCredential(
                "azure_c32c70c522d9ab59f85c145795bc2715@azure.com",
                "bknrare2912"
                 );

            //Crea un transport Web per enviar l'email
            var transportWeb = new Web(credentials);

            //Envia l'email
            if (transportWeb != null)
            {
                await transportWeb.DeliverAsync(myMessage);
            }
            else
            {
                Trace.TraceError("Failed to create Web transport.");
                await Task.FromResult(0);
            }
        }
        public static async Task SendContactEmail(int id, string subject, string body)
        {
            //Crea l'objecte de l'email
            var myMessage = new SendGridMessage();
            //Crea l'usuari destinatari
            User senderUser = GetUser(id);

            //Afegeix les propietats al missatge
            myMessage.From = new MailAddress(senderUser.userMail);
            ;

            myMessage.AddTo("suportcommunitime@gmail.com");

            myMessage.Subject = "User " + senderUser.userID.ToString() + subject;

            //AAfegeix el cos en text i HTML
            myMessage.Html = body;
            myMessage.Text = body;

            //Introdueix les credencials de l'aplicació externa SendGrid
            var credentials = new NetworkCredential(
                "azure_c32c70c522d9ab59f85c145795bc2715@azure.com",
                "bknrare2912"
                 );

            //Crea un transport Web per enviar l'email
            var transportWeb = new Web(credentials);

            //Envia l'email
            if (transportWeb != null)
            {
                await transportWeb.DeliverAsync(myMessage);
            }
            else
            {
                Trace.TraceError("Failed to create Web transport.");
                await Task.FromResult(0);
            }
        }
        public static void ChangePassword(int userID, string password)
        {
            //Si l'usuari existeix
            if (DoesUserExist(GetUser(userID)))
            {
                //Crea una nova clau a partir de la contrasenya
                byte[] newKey = PasswordManager.Hash(password);
                //Utilitzant l'accés "ctx" a la base de dades
                using (var ctx = new DomainContext())
                {
                    //Assigna la nova clau a l'usuari
                    ctx.Users.FirstOrDefault(u => u.userID == userID).userKey = newKey;
                    //Desa els canvis
                    ctx.SaveChanges();
                }
            }
        }




    }


}
