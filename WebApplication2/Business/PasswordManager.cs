﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainProject;
using System.Security.Cryptography;

namespace Business
{
    public static class PasswordManager
    {
        public static byte[] Hash(string password)
        {
            //Utilitzant una encriptació de 20 bytes per aquesta contrasenya
            using (var deriveBytes = new System.Security.Cryptography.Rfc2898DeriveBytes(password, 20))
            {
                //Genera un conjunt de bytes aleatoris (el Salt)
                byte[] salt = deriveBytes.Salt;
                //Genera un conjunt de bytes a partir de la contrasenya (el Hash)
                byte[] hash = deriveBytes.GetBytes(20);

                //Junta en un sol conjunt de bytes el Hash i el Salt i retorna'l
                return salt.Concat(hash).ToArray();
            }
        }
        public static bool IsPasswordValid(User user, string password)
        {
            //Agafa el Salt dels primers 20 bytes de la clau de l'usuari
            byte[] salt = user.userKey.Take(20).ToArray();
            //Agafa el Hash dels últims 20 bytes de la clau de l'usuari
            byte[] key = user.userKey.ToList().GetRange(20,20).ToArray();
            //Fa passar la contrasenya que ha escrit l'usuari per la mateixa encriptació
            //que quan se va crear el compte.
            using (var deriveBytes = new Rfc2898DeriveBytes(password, salt))
            {
                //Genera un conjunt de 20 bytes a partir de la contrasenya
                byte[] newKey = deriveBytes.GetBytes(20);
                //Compara la nova clau amb la clau de l'usuari. Si son iguals, retorna true.
                //Si no, retorna false
                return newKey.SequenceEqual(key);
            }
        }

    }
}
