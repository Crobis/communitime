﻿using System;
using DomainProject;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public static class OfferManager
    {
        public static void SaveOffer(Offer offer)
        {
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
            {
                //Afegeix l'oferta a la base de dades i desa els canvis
                ctx.Offers.Add(offer);
                ctx.SaveChanges();
            }
        }
        public static List<Offer> GetOffers(string search = "")
        {
            //Declara una llista d'ofertes
            List<Offer> Offers = new List<Offer>();
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
            {
                //Si no s'ha cercat res:
                if (string.IsNullOrWhiteSpace(search))
                    //Assigna totes les ofertes
                    Offers = ctx.Offers.ToList();
                //Si s'ha cercat alguna cosa:
                else
                    //Assigna les ofertes que coincideixin amb la cerca
                    Offers = ctx.Offers.Where(n => n.offerDescription.Contains(search) || n.offerName.Contains(search) || n.offerTags.Contains(search)).ToList();
            }
            //Retorna la llista d'ofertes
            return Offers;
        }
        public static Offer GetOffer(int id)
        {
            //Declara una oferta
            Offer offer = new Offer();
            //Utilitzant l´accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
                //Assigna l'oferta que tingui la id que ha rebut el mètode
                offer = ctx.Offers.FirstOrDefault(n => n.offerID == id);
            //Retorna l'oferta
            return offer;
        }
        public static List<Offer> GetSimilarOffers(Offer o1)
        {
            //Declara una llista d'ofertes
            List<Offer> offers;
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
                //Assigna totes les ofertes a la llista
                offers = ctx.Offers.ToList();

            //Elimina de la llista l'oferta de la què es busquen similars
            offers.RemoveAll(o => o.offerID == o1.offerID);
            //Ordena per similitud a "o1" i de forma descendent de la resta d'ofertes
            offers = offers.OrderByDescending(o => OfferComparison(o1, o)).ToList();
            //Elimina totes menys les tres primeres
            try { offers.RemoveRange(BaseManager.MaxSimilarShow, offers.Count - BaseManager.MaxSimilarShow); } catch { }
            //Retorna la llista d'ofertes
            return offers;
        }
        public static List<Demand> GetSimilarDemands(Offer o)
        {
            //Declara una llista de demandes
            List<Demand> demands;
            //Assigna totes les demandes a la llista
            demands = DemandManager.GetDemands();
            //Ordena la llista de forma descendent i per similitud amb "o"
            demands = demands.OrderByDescending(d => OfferComparison(o, d)).ToList();
            //Elimina de la llista les demandes que siguin de l'usuari
            demands.RemoveAll(d => d.userID == o.userID);
            //Elimina totes les ofertes excepte les 3 primeres
            try{ demands.RemoveRange(BaseManager.MaxSimilarShow, demands.Count - BaseManager.MaxSimilarShow); } catch { }
            //Retorna la llista d'ofertes
            return demands;
        }
        public static int OfferComparison(Offer o1, Offer o2)
        {
            //Defineix dues llistes de paraules: el títol i les etiquetes
            List<string> tags, title;
            //Separa el títol i les etiquetes de la demanda "o1" per paraules
            tags = o1.offerTags.Split(',').ToList();
            title = o1.offerName.Split(' ').ToList();
            //Declara la similitud entre "o1" i "o2" amb valor 0
            int similarity = 0;
            //Declara la cadena de caràcters d'"o2" amb què es compararan les paraules d'"o1"
            string offerString = o2.offerName + o2.offerDescription + o2.offerTags;
            //Per cada paraula "s" al títol
            foreach (string s in title)
                //Si "offerString" conté la paraula "s":
                if (s.Count<char>() > 3 && !string.IsNullOrWhiteSpace(s) && offerString.Contains(s))
                    //Suma 1 a "similarity"
                    similarity++;
            //Per cada paraula "s" a les etiquetes
            foreach (string s in tags)
                //Si "offerString" conté la paraula "s"
                if (s.Count<char>() > 3 && !string.IsNullOrWhiteSpace(s) && offerString.Contains(s))
                    //Suma 2 a "similarity"
                    similarity += 2;
            //Retorna la similitud entre les dues demandes
            return similarity;
        }
        public static int OfferComparison(Offer o, Demand d)
        {
            //Defineix dues llistes de paraules: el títol i les etiquetes
            List<string> tags, title;
            //Separa el títol i les etiquetes de l'oferta "o" per paraules
            tags = o.offerTags.Split(',').ToList();
            title = o.offerName.Split(' ').ToList();
            //Declara la similitud entre la demanda i l'oferta a 0
            int similarity = 0;
            //Declara la cadena de caràcter de "d" amb què es compararan les paraules d'"o"
            string demandString = d.demandName + d.demandDescription + d.demandTags;
            //Per cada paraula "s" al títol
            foreach (string s in title)
                //Si "offerString" conté la paraula "s":
                if (s.Count<char>() > 3 && !string.IsNullOrWhiteSpace(s) && demandString.Contains(s))
                    //Suma 1 a "similarity"
                    similarity++;
            //Per cada paraula "s" a les etiquetes:
            foreach (string s in tags)
                //Si "offerString" conté la paraula "s":
                if (s.Count<char>() > 3 && !string.IsNullOrWhiteSpace(s) && demandString.Contains(s))
                    //Suma 2 a "similarity"
                    similarity += 2;
            //Retorna la similitud entre "d" i "o"
            return similarity;
        }
        public static void EditOffer (Offer o)
        {
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
            {
                //Canvia els valors antics de l'oferta pels valors nous rebuts des del controller
                System.IO.File.Delete(ctx.Offers.FirstOrDefault(of => of.offerID == o.offerID).offerImagePath);
                ctx.Offers.FirstOrDefault(of => of.offerID == o.offerID).offerName = o.offerName;
                ctx.Offers.FirstOrDefault(of => of.offerID == o.offerID).offerImagePath = o.offerImagePath;
                ctx.Offers.FirstOrDefault(of => of.offerID == o.offerID).offerDescription = o.offerDescription;
                ctx.Offers.FirstOrDefault(of => of.offerID == o.offerID).offerTags= o.offerTags;
                //Desa els canvis
                ctx.SaveChanges();
            }
        }
        public static List<Offer> GetHomeOffers ()
        {
            //Declara dues llistes d'ofertes un generador de nombres aleatoris i una demanda
            List<Offer> offers = new List<Offer>();
            List<Offer> homeOffers = new List<Offer>();
            Random r = new Random();
            Offer offer;

            //Assigna totes les ofertes a la primera llista d'ofertes
            offers = GetOffers();

            try
            {
                //Afegeix com a màxim 3 ofertes a l'atzar a la llista d'ofertes
                for (int i = 0; i < ConstantClass.MaxHomeOfferDemands && i < offers.Count; i++)
                {
                    //Agafa una oferta a l'atzar entre totes les que hi ha
                    offer = offers[r.Next(offers.Count)];
                    //Si l'oferta no ha estat seleccionada abans:
                    if (!homeOffers.Any(o => o.offerID == offer.offerID))
                        //Afegeix-la a la llista d'ofertes de la pàgina principal
                        homeOffers.Add(offer);
                    //Si ho ha estat, torna-ho a intentar
                    else i--;
                }
            } catch { }
            //Retorna la llista de demandes a la pàgina principal
            return homeOffers;
        }
        public static double GetDistance (User user, Offer offer)
        {
            //Emmagatzema en variables la localització de l'usuari connectat i la de l'usuari que ha creat l'oferta
            string loc1 = user.userCity.TrimEnd(' ');
            string loc2 = UserManager.GetUser(offer.userID).userCity.TrimEnd(' ');
            //Crida el JSONManager per aconseguir la distància entre les localitzacions
            return JSONManager.GetDistance(loc1, loc2);
        }
    }
}
