﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainProject;
using System.Threading.Tasks;

namespace Business
{
    public class DemandManager
    {
        public static void SaveDemand(Demand demand)
        {
            //Declara una variable ctx que serveix d'accés a la base de dades
            using (var ctx = new DomainContext())
            {
                //Afegeix la demanda a la base de dades i desa els canvis
                ctx.Demands.Add(demand);
                ctx.SaveChanges();
            }
        }
        public static List<Demand> GetDemands(string search = "")
        {
            //Declara una llista de demandes
            List<Demand> Demands = new List<Demand>();
            //Utilitzant l'accés a la base de dades
            using (var ctx = new DomainContext())
            {
                //Si no s'ha cercat res:
                if (string.IsNullOrWhiteSpace(search))
                    //Assigna totes les demandes
                    Demands = ctx.Demands.ToList();
                //Si s'ha cercat alguna cosa:
                else
                    //Assigna les demandes que coincideixin amb la cerca
                    Demands = ctx.Demands.Where(n => n.demandDescription.Contains(search) || n.demandName.Contains(search) || n.demandTags.Contains(search)).ToList();
            }
            //Retorna la llista de demandes
            return Demands;
        }
        public static Demand GetDemand(int id)
        {
            //Declara una demanda
            Demand demand = new Demand();
            //Utilitzant l'accés a la base de dades
            using (var ctx = new DomainContext())
                //Assigna la demanda que tingui la id que ha rebut el mètode
                demand = ctx.Demands.FirstOrDefault(n => n.demandID == id);
            //Retorna la demanda
            return demand;
        }
        public static List<Demand> GetHomeDemands()
        {
            //Declara dues llistes de demandes, un generador de nombres aleatoris i una demanda
            List<Demand> demands = new List<Demand>();
            List<Demand> homeDemands = new List<Demand>();
            Random r = new Random();
            Demand demand;

            //Assigna totes les demandes a la primera llista de demandes
            demands = GetDemands();

            try
            {
                //Afegeix com a màxim 3 demandes a l'atzar a la llista de demandes de la pàgina principal
                for (int i = 0; i < ConstantClass.MaxHomeOfferDemands && i < demands.Count; i++)
                {
                    //Agafa una demanda a l'atzar d'entre totes les que hi ha
                    demand = demands[r.Next(demands.Count)];
                    //Si la demanda no ha estat seleccionada abans:
                    if (!homeDemands.Any(d => d.demandID == demand.demandID))
                        //Afegeix-la a la llista de demandes de la pàgina principal
                        homeDemands.Add(demand);
                    //Si ho ha estat, torna-ho a intentar
                    else i--;

                }
            }
            catch { }
            //Retorna la llista de demandes a la pàgina principal
            return homeDemands;
        }
        public static List<Demand> GetSimilarDemands(Demand d1)
        {
            //Declara una llista de demandes
            List<Demand> demands;
            //Assigna totes les demandes a la llista
            demands = GetDemands();

            //Elimina de la llista la demanda de la què es busquen similars
            demands.RemoveAll(d => d.demandID == d1.demandID);
            //Ordena per similitud a "d1" i de forma descendent la resta de demandes 
            demands = demands.OrderByDescending(d => DemandComparison(d1, d)).ToList();
            //Elimina totes menys les tres primeres
            demands.RemoveRange(BaseManager.MaxSimilarShow, demands.Count - BaseManager.MaxSimilarShow);
            //Retorna la llista de demandes
            return demands;
        }
        public static List<Offer> GetSimilarOffers(Demand d)
        {
            //Declara una llista d'ofertes
            List<Offer> offers;
            //Assigna totes les ofertes a la llista
            offers = OfferManager.GetOffers();
            //Ordena la llista de forma descent i per similitud amb "d"
            offers = offers.OrderByDescending(o => DemandComparison(d, o)).ToList();
            //Elimina de la llista les ofertes que siguin de l'usuari
            offers.RemoveAll(o => o.userID == d.userID);
            //Elimina totes les ofertes excepte les 3 primeres
            try { offers.RemoveRange(BaseManager.MaxSimilarShow, offers.Count - BaseManager.MaxSimilarShow); } catch { }
            //Retorna la llista d'ofertes
            return offers;
        }
        public static int DemandComparison(Demand d1, Demand d2)
        {
            //Defineix dues llistes de paraules: el títol i les etiquetes
            List<string> tags, title;
            //Separa el títol i les etiquetes de la demanda "d1" per paraules
            tags = d1.demandTags.Split(',').ToList();
            title = d1.demandName.Split(' ').ToList();
            //Declara la similitud entre "d1" i "d2" amb valor 0.
            int similarity = 0;
            //Declara la cadena de caràcters de "d2" amb què es compararan les paraules de "d1"
            string demandString = d2.demandName + d2.demandDescription + d2.demandTags;

            //Per cada paraula "s" al títol
            foreach (string s in title)
                //Si "demandString" conté la paraula "s":
                if (s.Count<char>() > 3 && !string.IsNullOrWhiteSpace(s) && demandString.Contains(s))
                    //Suma-li 1 a "similarity"
                    similarity++;

            //Per cada paraula "s" a les etiquetes
            foreach (string s in tags)
                //Si "demandString" conté la paraula "s":
                if (s.Count<char>() > 3 && !string.IsNullOrWhiteSpace(s) && demandString.Contains(s))
                    //Suma-li 2 a "similarity"
                    similarity += 2;

            //Retorna la similitud entre les dues demandes
            return similarity;
        }
        public static int DemandComparison(Demand d, Offer o)
        {
            //Defineix dues llistes de paraules: el títol i les etiquetes
            List<string> tags, title;
            //Separa el títol i les etiquetes de la demanda "d" per paraules
            tags = d.demandTags.Split(',').ToList();
            title = d.demandName.Split(' ').ToList();
            //Declara la similitud entre l'oferta i la demanda a 0
            int similarity = 0;
            //Declara la cadena de caràcters d'"o" amb què es compararan les paraules de "d1"
            string offerString = o.offerName + o.offerDescription + o.offerTags;
            //Per cada paraula "s" al títol
            foreach (string s in title)
                //Si "offerString" conté la paraula "s":
                if (s.Count<char>() > 3 && !string.IsNullOrWhiteSpace(s) && offerString.Contains(s))
                    //Suma 1 a "similarity"
                    similarity++;

            //Per cada paraula "s" a les etiquetes:
            foreach (string s in tags)
                //Si "offerString" conté la paraula "s":
                if (s.Count<char>() > 3 && !string.IsNullOrWhiteSpace(s) && offerString.Contains(s))
                    //Suma 2 a "similarity"
                    similarity += 2;

            //Retorna la similitud entre "d" i "o"
            return similarity;
        }
        public static void EditDemand(Demand d)
        {
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
            {
                //Canvia els valors antics de la demanda pels valors nous rebuts des del controller
                System.IO.File.Delete(ctx.Demands.FirstOrDefault(de => de.demandID == d.demandID).demandImagePath);
                ctx.Demands.FirstOrDefault(de => de.demandID == d.demandID).demandName = d.demandName;
                ctx.Demands.FirstOrDefault(de => de.demandID == d.demandID).demandImagePath = d.demandImagePath;
                ctx.Demands.FirstOrDefault(de => de.demandID == d.demandID).demandDescription = d.demandDescription;
                ctx.Demands.FirstOrDefault(de => de.demandID == d.demandID).demandTags = d.demandTags;
                //Desa els canvis
                ctx.SaveChanges();
            }
        }
        public static double GetDistance(User user, Demand demand)
        {
            //Emmagatzema en variables la localització de l'usuari connectat i de l'usuari que ha creat la demanda
            string loc1 = user.userCity.TrimEnd(' ');
            string loc2 = UserManager.GetUser(demand.userID).userCity.TrimEnd(' ');
            //Crida el JSONManager per aconseguir la distància entre les dues localitzacions
            return JSONManager.GetDistance(loc1, loc2);
        }
    }

}
