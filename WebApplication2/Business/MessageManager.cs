﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using DomainProject;

namespace Business
{
    public class MessageManager
    {
        static RNGCryptoServiceProvider rngCSP = new RNGCryptoServiceProvider();
        public static void SaveMessage(Message message)
        {
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
            {
                //Afegeix el missatge a la base de dades i desa els canvis
                ctx.Messages.Add(message);
                ctx.SaveChanges();
            }
        }
        public static List<Message> GetMessages(int groupID)
        {
            //Declara una nova llista de missatges
            List<Message> messages = new List<Message>();
            //Si el grup existeix:
            if (GroupManager.DoesGroupExist(groupID))
                //Utilitzant l'accés "ctx" a la base de dades
                using (var ctx = new DomainContext())
                    //Assigna a la llista els missatges que pertanyin al grup
                    messages = ctx.Messages.Where(m => m.chatGroupID == groupID).ToList();
            //Retorna la llista
            return messages;
        }
        public static List<Message> GetNewMessages(int userID)
        {
            //Declara una llista de grups i una llista de missatges
            List<ChatGroup> chatGroups = new List<ChatGroup>();
            List<Message> messages = new List<Message>();
            //Assigna a la llista de grups els que pertanyin a l'usuari
            chatGroups = GroupManager.GetGroups(userID);
            //Per cada grup de l'usuari:
            foreach (var group in chatGroups)
                //Utilitzant l'accés "ctx" a la base de dades:
                using (var ctx = new DomainContext())
                    //Afegeix els missatges no llegits per l'usuari d'aquest grup
                    messages.AddRange(ctx.Messages.Where(m => m.chatGroupID == group.chatGroupID && !m.messageIsRead && m.messageFromUserID != userID).ToList());
            //Retorna tots els missatges no llegits de l'usuari
            return messages;
        }
        
    }
}
