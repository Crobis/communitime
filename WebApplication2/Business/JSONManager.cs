﻿using DomainProject;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public static class JSONManager
    {
        public static string getJson (string url)
        {
            //Crea una petició web a partir de l'url
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                //Rep la resposta a la petició
                WebResponse response = request.GetResponse();
                //Llegeix la resposta i la transforma en un JSON
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            //Si surt res malamant en el procés, retorna el missatge d'error
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }
        public static double GetDistance (string loc1, string loc2)
        {
            //Demana un JSON que inclogui la distància entre les dues localitzacions
            string json = getJson("http://www.mapquestapi.com/directions/v2/route?key=qZBBHQ3IAwnKIiuWJC3T2yBYABCrU5IA&from=" + loc1 + "&to=" + loc2);
            //Desserialitza el JSON
            RootObject root = JsonConvert.DeserializeObject<RootObject>(json);
            //Retorna la distància
            return root.route.distance;
        }
    }
}
