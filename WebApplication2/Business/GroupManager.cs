﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainProject;

namespace Business
{
    public class GroupManager
    {
        public static void SaveGroup(ChatGroup cGroup)
        {
            //Utilitzant l'accés "ctx" a la base de dades:
            using (var ctx = new DomainContext())
            {
                //Afegeix el grup a la base de dades i desa els canvis
                ctx.ChatGroups.Add(cGroup);
                ctx.SaveChanges();
            }
        }
        public static bool DoesGroupExist(int id)
        {
            //Utilitzant l'accés "ctx" a la base de dades:
            using (var ctx = new DomainContext())
                //Retorna true si hi ha cap grup la "chatGroupID" del qual sigui igual a "id", false en cas contrari
                return ctx.ChatGroups.Any(g => g.chatGroupID == id);
        }
        public static bool DoesGroupExist(int offOrDemID, DemandOrOffer demOrOff, int userCreatorID)
        {
            //Utilitzant l'accés "ctx" a la base de dades:
            using (var ctx = new DomainContext())
                //Retorna true si hi ha cap grup amb els mateixos valors que els rebuts pel mètode, false en cas contrari
                return ctx.ChatGroups.Any(g => g.offerOrDemandID == offOrDemID && g.demOrOffInt == (int)demOrOff && g.userCreatorID == userCreatorID);
        }
        public static List<ChatGroup> GetGroups()
        {
            //Declara una llista de grups
            List<ChatGroup> groups = new List<ChatGroup>();
            //Utilitzant l'accés "ctx" a la base de dades
            using (var ctx = new DomainContext())
            {
                //Assigna tots els grups a la llista
                groups = ctx.ChatGroups.ToList();
            }
            //Retorna la llista de grups
            return groups;
        }
        public static List<ChatGroup> GetGroups(int userID)
        {
            //Declar a una llista d'ofertes, una de demandes i una de grups
            List<Offer> offers = new List<Offer>();
            List<Demand> demands = new List<Demand>();
            List<ChatGroup> groups = new List<ChatGroup>();
            
            //Si l'usuari del qual es demanen els grups existeix:
            if (UserManager.DoesUserExist(UserManager.GetUser(userID)))
            {
                //La llista d'ofertes i la de demandes són iguals a les seves ofertes i demandes
                offers = UserManager.GetUserOffers(userID);
                demands = UserManager.GetUserDemands(userID);
                //Utilitzant l'accés "ctx" a la base de dades
                using (var ctx = new DomainContext())
                {
                    //La llista de grups és igual als grups que l'usuari ha creat
                    groups = ctx.ChatGroups.Where(g => g.userCreatorID == userID).ToList();
                    //Per cada oferta de l'usuari
                    foreach (var offer in offers)
                        //Afegeix tots els grups relacionats amb aquesta oferta
                        groups.AddRange(ctx.ChatGroups.Where(g => g.offerOrDemandID == offer.offerID && g.demOrOffInt == (int)DemandOrOffer.Offer).ToList());
                    //Per cada demanda de l'usuari
                    foreach (var demand in demands)
                        //Afegeix tots els grups relacionats amb aquesta demanda
                        groups.AddRange(ctx.ChatGroups.Where(g => g.offerOrDemandID == demand.demandID && g.demOrOffInt == (int)DemandOrOffer.Demand).ToList());
                }
            }
            //Retorna la llista de grups
            return groups;
        }
        public static ChatGroup GetGroup(int id)
        {
            //Declara un nou grup
            ChatGroup group = new ChatGroup();
            //Utilitzant l'accés "ctx" a la base de dades:
            using (var ctx = new DomainContext())
                //El grup és igual a aquell que tingui una "chatGroupID" igual a "id"
                group = ctx.ChatGroups.FirstOrDefault(g => g.chatGroupID == id);
            //Retorna el grup
            return group;
        }
        public static ChatGroup GetGroup(int offOrDemID, DemandOrOffer demOrOff, int userCreatorID)
        {
            //Declara un nou grup
            ChatGroup group = new ChatGroup();
            //Utilitzant l'accés "ctx" a la base de dades:
            using (var ctx = new DomainContext())
                //El grup és igual amb els que els paràmetres utilitzats pels mètode coincideixin
                group = ctx.ChatGroups.FirstOrDefault(g => g.offerOrDemandID == offOrDemID && g.demOrOffInt == (int)demOrOff && g.userCreatorID == userCreatorID);
            //Retorna el grup
            return group;
        }
        public static void ReadMessages(int groupID, int userID)
        {
            //Utilitzant l'accés "ctx" a la base de dades:
            using (var ctx = new DomainContext())
            {
                //Per cada missatge dins del grup que hagi estat enviat a l'usuari:
                foreach (var m in ctx.Messages)
                {
                    if (!m.messageIsRead && m.messageFromUserID != userID && m.chatGroupID == groupID)
                        //Marca el missatge com a llegit
                        m.messageIsRead = true;
                }
                //Desa els canvis
                ctx.SaveChanges();
            }
        }
        public static Message GetLastMessage (int groupID)
        {
            //Declara un nou missatge
            Message message = new Message();
            try
            {
                //El missatge és igual a l'últim enviat pel grup
                message = MessageManager.GetMessages(groupID).Last();
            }
            catch { }
            //Retorna el missatge
            return message;
        }
        public static string[] GetOfferOrDemandInfo (ChatGroup group)
        {
            switch (group.demOrOffInt)
            {
                //Si el grup està assignat a una demanda:
                case (int)DemandOrOffer.Demand:
                    //Retorna la informació d'aquesta demanda
                    Demand dem = DemandManager.GetDemand(group.offerOrDemandID);
                    string[] demInfo = { dem.demandName, dem.demandImagePath, dem.demandDescription };
                    return demInfo;
                //Si el grup està assignat a una oferta:
                case (int)DemandOrOffer.Offer:
                    //Retorna la informació d'aquesta oferta
                    Offer off = OfferManager.GetOffer(group.offerOrDemandID);
                    string[] offInfo = { off.offerName, off.offerImagePath, off.offerDescription };
                    return offInfo;
                //Si no es compleix cap dels dos casos:
                default:
                    //No retornis res
                    return null;
            }
        }
    }

}
